const express = require("express");
const app = express();
const cors = require("cors");

const { Sequelize, QueryTypes } = require("sequelize");

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const sequelize = new Sequelize({
  database: process.env.DB_DATABASE,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: process.env.DB_DIALECT
});

app.use(express.json());
app.use(cors());

// const entries = [];

app.get("/", (req, res) => {
  return res.status(200).send("Welcome to my API");
});

// A list of journals
app.get("/v1/api/journals", async (req, res) => {
  const apiResp = {
    data: [],
    error: ""
  };

  try {
    console.log("Connection established...");

    apiResp.data = await sequelize.query("SELECT * FROM journal LIMIT 10", {
      type: QueryTypes.SELECT
    });

    console.log(apiResp.data);
  } catch (e) {
    apiResp.error = e;
    console.error(e);
  }
  return res.status(200).json(apiResp);
});

// A list of entries
app.get("/v1/api/entries", async (req, res) => {
  const apiResp = {
    data: [],
    error: ""
  };

  try {
    console.log("Connection established...");

    apiResp.data = await sequelize.query(
      "SELECT * FROM journal_entry LIMIT 10",
      {
        type: QueryTypes.SELECT
      }
    );

    console.log(apiResp.data);
  } catch (e) {
    apiResp.error = e;
    console.error(e);
  }
  return res.status(200).json(apiResp);
});

//Route param: A journal entry
app.post("/v1/api/entries", async (req, res) => {
  const { entry } = req.body;

  if (!entry.title && !entry.content) {
    res.status(400).json({
      message: "Title and content can not be empty!"
    });
    return;
  }
  if (!entry.title) {
    res.status(400).json({
      message: "Title can not be empty!"
    });
    return;
  }
  if (!entry.content) {
    res.status(400).json({
      message: "Content can not be empty!"
    });
    return;
  }

  try {
    console.log("Connection established...");

    await sequelize.query(
      `INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ( :title, :content, :author_id, :journal_id )`,
      {
        type: QueryTypes.INSERT,
        replacements: {
          title: entry.title,
          content: entry.content,
          author_id: entry.author_id,
          journal_id: entry.journal_id
        }
      }
    );
    return res.status(200).json({ message: "Success" });
  } catch (error) {
    return res.status(500).send(error);
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server has started on ${PORT}...`);
});
